# README #

## PEmapper: a reference-based NGS short-read mapping program.

Cite: PEMapper and PECaller provide a simplified approach to whole-genome sequencing. PNAS 2017;114(10).  

## Your data files  
1. FASTQ: .fastq.gz (fwd and rev reads)  
2. sids.txt (list of sample ids)  
3. ped file (for pecaller2)
4. fam file (for plink)  

## Please contact Dave Cutler (djcutle at emory dot edu) for the following scripts and data files.  
1. Scripts:  
    pemapper2  
    pecaller2  
    merge_indel_snp_fixed.pl  
    snp_tran_counter_site.pl  
    snp_tran_silent_rep_site.pl  
    snp_filter_call_rate.pl  
    snp_to_vcf2  
    add_rs_to_vcf.pl  
    snp_to_linkage.pl  

2. Ref data: hg38.sdx, hg38_unique.bed  
3. Control data: 57 Control genomes (pileup.gz & indel.txt.gz)

## install 
1. csvkit (https://csvkit.readthedocs.io/en/latest/)  
2. bcftools  
3. plink  
4. king  

## job.1_mapper.sh  
## job.2_caller.sh  
## job.3_merge.sh  
## job.4_subset_snp.sh  
## job.5_post_variant_qc.sh
