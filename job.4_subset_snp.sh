#!/bin/sh

# submit this job from /logs folder
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N snp_subset
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q b.q
##$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 1
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

# csvkit is a suite of command-line tools for converting to and working with CSV
# pip install csvkit
# https://csvkit.readthedocs.io/en/latest/

# input.txt

# id	name
# 01	ashok
# 02	hari
# 03	hansi
# 04	sruja

# csvcut

# -c A comma separated list of column indices, names or ranges to be extracted, e.g. "1,id,3-5". Defaults to all columns.
# -l Insert a column of line numbers at the front of the output.
# -x, --delete-empty-rows
# -K Specify the number of initial lines to skip before the header row
# -H, --no-header-row 
# -d DELIMITER
# -t 

# csvformat

# -D OUT_DELIMITER
# -T

PROJ_DIR=/home/adinasarapu/zwick_rare/buz
IN_FILE=$PROJ_DIR/CallingResults/buz.merge.snp
SID_FILE=$PROJ_DIR/sids.txt
OUT_FILE=$PROJ_DIR/CallingResults/buz.final.snp

# Print the indices and names of all columns:
# csvcut -t -n buz/CallingResults/buz.merge.snp
# csvcut -t -n $In_FILE/buz.merge.snp > $PROJ_DIR/snp.columns.txt

MAIN_COLS='Fragment,Position,Reference,Alleles,Allele_Counts,Type'

# Bash Assign Output of Shell Command To Variable
idx=`csvcut -n -t $IN_FILE | grep -f $SID_FILE | csvcut -d ':' -c 1 | awk 'BEGIN { OFS = "," } {$2=$0+1;} {print $1,$2}' | paste -s -d','`

csvcut -t -c $MAIN_COLS,$idx $IN_FILE | csvformat -T > $OUT_FILE

echo "Stop - `date`"
