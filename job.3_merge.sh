#!/bin/sh

# submit this job from /logs folder

# merges the variant information contained in #####.snp (output from PECaller, job_caller.sh) with the 
# insertion/deletion information in the ###.indel.txt.gz files (output from PEMapper, job_mapper.sh) 
# PEMapper and PECaller provide a simplified approach to whole-genome sequencing. PNAS 2017;114(10).

echo "Start - `date`"

## job is rerunnable
#$ -r n
## name for the job 
#$ -N MergePEClr
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q b.q
##$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 9
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR=$HOME/zwick_rare
PID=buz
TYE_DIR=$PROJ_DIR/$PID/TYEDirectory

CALL_DIR=$PROJ_DIR/$PID/CallingResults
if [ ! -d "$CALL_DIR" ]; then
  mkdir -p "$CALL_DIR"
fi

SDX=$PROJ_DIR/hg38/hg38.sdx

ln -s $TYE_DIR/TYEDirectory.snp $CALL_DIR/${PID}.snp

OUT_MERGE=$CALL_DIR/${PID}.merge.snp
MERGE_INDEL=${PROJ_DIR}/script/merge_indel_snp_fixed.pl

$MERGE_INDEL \
	$SDX \
	$CALL_DIR/${PID}.snp \
	$TYE_DIR \
	$OUT_MERGE

echo "Finish - `date`"
